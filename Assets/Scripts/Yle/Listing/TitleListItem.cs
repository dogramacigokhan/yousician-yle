﻿using System.Linq;
using DG.Tweening;
using Extensions;
using TMPro;
using UniRx;
using UnityEngine;
using UnityEngine.UI;
using Yle.Http.Helpers;
using Yle.Http.Results;
using Yle.Settings;
using Zenject;

namespace Yle.Listing
{
    public interface ITitleListItem : IImageSetter
    {
        TitleInfo TitleInfo { get; set; }

        void Reset();
        void Init(TitleInfo info);
        void Despawn();
    }

    public class TitleListItem : MonoBehaviour, ITitleListItem
    {
        public Image Image;
        public TextMeshProUGUI ImageNotAvailable;
        public TextMeshProUGUI Title;

        public Button SelectionButton;

        public TitleInfo TitleInfo { get; set; }

        private ITitleListItemController _controller;
        private ImageSettings _imageSettings;

        [Inject]
        public void Construct(ITitleListItemController controller, ImageSettings imageSettings)
        {
            _controller = controller;
            _imageSettings = imageSettings;

            _controller.Init(this);
            Reset();
        }

        public void Reset()
        {
            if (Image != null)
            {
                Image.sprite = null;
                Image.SetAlpha(0);
                ImageNotAvailable.gameObject.SetActive(false);
            }

            if (Title != null)
            {
                Title.text = string.Empty;
            }
            gameObject.SetActive(true);
        }

        public void Init(TitleInfo titleInfo)
        {
            if (titleInfo == null)
            {
                Debug.LogError("Given title info is null for the list item!");
                return;
            }
            
            // Setup properties
            transform.SetAsLastSibling();
            TitleInfo = titleInfo;
            
            if (Title != null)
            {
                Title.text = TitleInfo.Title.First().Value;
            }
            
            // Setup the selection action
            SelectionButton.OnClickAsObservable()
                .Subscribe(_ => _controller.ShowInfo(TitleInfo))
                .AddTo(this);
            
            // Try to download the image
            _controller.DownloadImage(titleInfo);
        }

        public void Despawn()
        {
            // If controller is downloading the image, cancel it
            _controller.StopDownload();
            
            // Disable the game object when it is despawned
            gameObject.SetActive(false);
            
            // Release the sprite resources
            if (Image != null && Image.sprite != null)
            {
                Destroy(Image.sprite);
            }
        }

        public void SetImage(Sprite sprite)
        {
            if (sprite != null && Image != null)
            {
                Image.sprite = sprite;
                Image.DOFade(1f, _imageSettings.FadeDuration);
            }
        }

        public void SetImageAsNotAvailable()
        {
            if (Image != null)
            {
                // Hide image
                Image.SetAlpha(0f);
            }
            if (ImageNotAvailable != null)
            {
                // Show not available text
                ImageNotAvailable.gameObject.SetActive(true);
            }
        }

        public class Pool : MemoryPool<TitleInfo, ITitleListItem>
        {
            protected override void OnDespawned(ITitleListItem item)
            {
                base.OnDespawned(item);
                item.Despawn();
            }

            protected override void Reinitialize(TitleInfo info, ITitleListItem item)
            {
                base.Reinitialize(info, item);

                item.Reset();
                item.Init(info);
            }
        }
    }
}