﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using RestSharp.Contrib;
using UniRx;
using Yle.Http.Helpers;
using Yle.Http.Queries;
using Yle.Settings;

namespace Tests.Editor
{
    public class RequestTests : BaseUnitTextFixture
    {
        private RequestSettings _testSettings;

        [SetUp]
        public void SetUp()
        {
            // Create a dummy settings to be used in web requests
            _testSettings = MockRequestSettings();
        }

        [TearDown]
        public void TearDown()
        {
            _testSettings = null;
        }
        
        private static readonly List<string> TestUrls = new List<string>
        {
            "https://example.com",
            "https://example.com/",
            "https://example.com/request.json",
            "https://example.com/test/request.json",
        };

        private static readonly List<SearchQuery> SearchQueries = new List<SearchQuery>
        {
            new SearchQuery("test", 5, 10),
            new SearchQuery("test2", 1, 10),
            new SearchQuery("some test", 5, 20)
        };
        
        private static readonly List<string> TitleIds = new List<string>
        {
            "1-2862718",
            "1-84654-465321",
            "464-3446566-1324687986",
        };

        [TestCaseSource(nameof(TestUrls))]
        public void Request_AppendsAppIdToRequest(string url)
        {
            // Arrange
            var requester = new Request(_testSettings);

            // Act
            var uriBuilder = new UriBuilder(url);
            var request = requester.AppendAuthenticationInfo(uriBuilder.Uri);
            var parsedQuery = HttpUtility.ParseQueryString(request.Query, Encoding.UTF8);

            // Assert
            Assert.That(parsedQuery[_testSettings.AppIdKey], Is.Not.Null);
            Assert.That(parsedQuery[_testSettings.AppIdKey], Is.Not.Empty);
        }

        [TestCaseSource(nameof(TestUrls))]
        public void Request_AppendsAppKeyToRequest(string url)
        {
            // Arrange
            var requester = new Request(_testSettings);

            // Act
            var uriBuilder = new UriBuilder(url);
            var request = requester.AppendAuthenticationInfo(uriBuilder.Uri);
            var parsedQuery = HttpUtility.ParseQueryString(request.Query, Encoding.UTF8);

            // Assert
            Assert.That(parsedQuery[_testSettings.AppKeyKey], Is.Not.Null);
            Assert.That(parsedQuery[_testSettings.AppKeyKey], Is.Not.Empty);
        }

        [TestCaseSource(nameof(SearchQueries))]
        public void Request_CanCreateSearchRequest(SearchQuery searchQuery)
        {
            // Arrange
            var requester = new Request(_testSettings);

            // Act
            var uri = requester.CreateSearchUri(searchQuery);
            var parsedQuery = HttpUtility.ParseQueryString(uri.Query, Encoding.UTF8);
            
            // Assert
            Assert.That(parsedQuery[_testSettings.QueryKey], Is.Not.Null);
            Assert.That(parsedQuery[_testSettings.QueryKey], Is.Not.Empty);
            Assert.That(parsedQuery[_testSettings.QueryKey], Is.EqualTo(searchQuery.Query));
            
            Assert.That(parsedQuery[_testSettings.LimitKey], Is.EqualTo(searchQuery.Limit.ToString()));
            Assert.That(parsedQuery[_testSettings.OffsetKey], Is.EqualTo(searchQuery.Offset.ToString()));
        }
        
        [TestCaseSource(nameof(TitleIds))]
        public void Request_CanCreateItemRequest(string titleId)
        {
            // Arrange
            var requester = new Request(_testSettings);

            // Act
            var uri = requester.CreateTitleUri(titleId);
            
            // Assert
            Assert.That(uri.Segments.Last(), Contains.Substring(titleId));
        }

        [TestCaseSource(nameof(SearchQueries))]
        public void Request_CanSearchItems(SearchQuery searchQuery)
        {
            // Arrange
            var requester = new Request(_testSettings);

            // Act
            var obs = requester.SearchItems(searchQuery).Subscribe();
            
            // Assert
            Assert.That(obs, Is.Not.Null);
            obs.Dispose();
        }
        
        [TestCaseSource(nameof(TitleIds))]
        public void Request_CanRetrieveTitles(string titleId)
        {
            // Arrange
            var requester = new Request(_testSettings);

            // Act
            var obs = requester.GetTitle(titleId).Subscribe();
            
            // Assert
            Assert.That(obs, Is.Not.Null);
            obs.Dispose();
        }
    }
}