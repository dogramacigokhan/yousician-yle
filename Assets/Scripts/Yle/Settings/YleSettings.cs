﻿using UnityEngine;
using Zenject;

namespace Yle.Settings
{
    [CreateAssetMenu(fileName = "YleSettings", menuName = "Settings/Yle Settings")]
    public class YleSettings : ScriptableObjectInstaller<YleSettings>
    {
        public RequestSettings Request;
        public TitleListingSettings TitleListing;
        public ImageSettings Image;
        public ScreenSettings Screen;
        public LanguageSettings Language;

        public override void InstallBindings()
        {
            // Bind global settings
            Container.BindInstance(this);
            
            Container.BindInstance(Request);
            Container.BindInstance(TitleListing);
            Container.BindInstance(Image);
            Container.BindInstance(Screen);
            Container.BindInstance(Language);
        }
    }
}