﻿using TMPro;
using UniRx;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Yle.Listing
{
    public interface ITitleListScreen
    {
        void ResetScrollRect();
        void ToggleVisibility(bool to);
        void ToggleLoadingBar(bool to);
    }

    public class TitleListScreen : MonoBehaviour, ITitleListScreen
    {
        public ScrollRect ScrollRect;
        public GameObject LoadingBar;
        
        private ITitleListController _controller;

        [Inject]
        public void Construct(ITitleListController controller)
        {
            _controller = controller;
        }

        private void Awake()
        {
            ScrollRect.ObserveEveryValueChanged(r => r.verticalNormalizedPosition)
                .Where(pos => pos <= 0)
                .Subscribe(_ => _controller.LoadNewItems())
                .AddTo(this);
        }

        private void OnDestroy()
        {
            _controller.Destroy();
        }

        public void ResetScrollRect()
        {
            if (ScrollRect != null)
            {
                ScrollRect.verticalNormalizedPosition = 1f;
            }
        }

        public void ToggleVisibility(bool to)
        {
            gameObject.SetActive(to);
        }

        public void ToggleLoadingBar(bool to)
        {
            if (LoadingBar != null)
            {
                LoadingBar.SetActive(to);
            }
        }
    }
}