﻿using System.Text.RegularExpressions;
using Moq;
using NUnit.Framework;
using UnityEngine;
using Yle.Http.Helpers;
using Yle.InfoScreen;
using Yle.Listing;
using Yle.Settings;
using Zenject;

namespace Tests.Editor
{
    public class TitleListItemTests : BaseUnitTextFixture
    {
        [SetUp]
        public void SetUp()
        {
            // Bind required interfaces and classes
            Container.Bind<IRequest>().FromMock().AsSingle();
            Container.Bind<ITitleListItem>().FromMock().AsSingle();
            Container.Bind<ImageSettings>().FromMock().AsSingle();
            Container.Bind<ITitleInfoController>().FromMock().AsSingle();
            Container.Bind<ImageDownloader>().AsSingle();
        }

        [TearDown]
        public void TearDown()
        {
            Container.UnbindAll();
        }

        [Test]
        public void TitleListItem_SetNotAvailableImage_IfTitleInfoIsNull()
        {
            // Arrange
            var mockController = MockTitleListItemController(true);
            var mockItem = Mock.Get(Container.Resolve<ITitleListItem>());
            
            // Act
            mockController.Object.DownloadImage(null);

            // Assert
            UnityEngine.TestTools.LogAssert.Expect(LogType.Warning, new Regex(".*"));
            mockItem.Verify(i => i.SetImageAsNotAvailable(), Times.Once);
        }
        
        [Test]
        public void TitleListItem_SetNotAvailableImage_IfSpriteIsNull()
        {
            // Arrange
            var mockItem = Mock.Get(Container.Resolve<ITitleListItem>());
            var mockImageDownloader = MockImageDownloader(true);
            
            // Act
            mockImageDownloader.Object.OnDownloadComplete(null, mockItem.Object);

            // Assert
            UnityEngine.TestTools.LogAssert.Expect(LogType.Error, new Regex(".*"));
            mockImageDownloader.Verify(c => c.OnDownloadComplete(null, mockItem.Object));
            mockItem.Verify(i => i.SetImageAsNotAvailable(), Times.Once);
        }
        
        [Test]
        public void TitleListItem_SetNotAvailableImage_OnDownloadError()
        {
            // Arrange
            var mockController = MockTitleListItemController(true);
            var mockItem = Mock.Get(Container.Resolve<ITitleListItem>());
            var mockImageDownloader = MockImageDownloader(true);
            
            // Act
            mockImageDownloader.Object.OnDownloadError(null, mockItem.Object);

            // Assert
            UnityEngine.TestTools.LogAssert.Expect(LogType.Exception, new Regex(".*"));
            mockImageDownloader.Verify(c => c.OnDownloadError(null, mockItem.Object));
            mockItem.Verify(i => i.SetImageAsNotAvailable(), Times.Once);
        }
        
        [Test]
        public void TitleListItem_SetImage_OnDownloadComplete()
        {
            // Arrange
            var mockItem = Mock.Get(Container.Resolve<ITitleListItem>());
            var mockImageDownloader = MockImageDownloader(true);

            var texture = Texture2D.blackTexture;
            var sprite = Sprite.Create(texture, Rect.zero, Vector2.zero);
            
            // Act
            mockImageDownloader.Object.OnDownloadComplete(sprite, mockItem.Object);

            // Assert
            mockImageDownloader.Verify(c => c.OnDownloadComplete(sprite, mockItem.Object));
            mockItem.Verify(i => i.SetImage(sprite), Times.Once);
            
            // Cleanup
            Object.DestroyImmediate(sprite);
        }
    }
}