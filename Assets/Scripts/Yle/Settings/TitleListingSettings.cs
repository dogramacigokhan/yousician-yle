﻿using System;
using UnityEngine;

namespace Yle.Settings
{
    [Serializable]
    public class TitleListingSettings
    {
        public GameObject TitleListItemPrefab;
        public int ItemSearchLimit;
    }
}