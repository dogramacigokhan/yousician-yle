﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using UnityEngine;
using Yle.Http.Results;
using Yle.Settings;

namespace Yle.Http.Helpers
{
    public class ResultHelper
    {
        private readonly LanguageSettings _languageSettings;

        public ResultHelper(LanguageSettings languageSettings)
        {
            _languageSettings = languageSettings;
        }
        
        public string GetLocalizedValue(Dictionary<string, string> values, string fallbackValue = "")
        {
            string result;
            if (values.TryGetValue(_languageSettings.DefaultLanguageCode, out result))
            {
                return result;
            }
            
            if (values.TryGetValue(_languageSettings.FallbackLanguageCode, out result))
            {
                return result;
            }

            if (values.Count > 0)
            {
                return values.First().Value;
            }

            return fallbackValue ?? "Unknown";
        }

        public static string GetAgeRestriction(ItemContentRating contentRating)
        {
            return contentRating?.AgeRestriction == 0 ? "No age restriction" : contentRating?.AgeRestriction + "+";
        }

        public static string GetCountry(string[] countryOfOrigin)
        {
            return countryOfOrigin != null && countryOfOrigin.Length > 0 ? string.Join(",", countryOfOrigin) : "Unknown";
        }

        public static string Iso8601ToString(string isoTime)
        {
            try
            {
                return XmlConvert.ToTimeSpan(isoTime).ToString("h'h 'm'm'");
            }
            catch (Exception e)
            {
                Debug.LogException(e);
                return isoTime;
            }
        }

    }
}