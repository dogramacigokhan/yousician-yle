﻿using System;
using Yle.Http.Helpers;
using Yle.Http.Results;
using Yle.InfoScreen;
using Yle.Settings;

namespace Yle.Listing
{
    public interface ITitleListItemController
    {
        void Init(ITitleListItem item);
        void DownloadImage(TitleInfo info);
        void StopDownload();
        void ShowInfo(TitleInfo info);
    }
    
    public class TitleListItemController : ITitleListItemController
    {
        private readonly ImageSettings _imageSettings;
        private readonly ITitleInfoController _titleInfoController;
        private readonly ImageDownloader _imageDownloader;
        
        private ITitleListItem _item;
        private IDisposable _imgDownload;

        public TitleListItemController(
            ImageSettings imageSettings,
            ITitleInfoController titleInfoController,
            ImageDownloader imageDownloader)
        {
            _imageSettings = imageSettings;
            _titleInfoController = titleInfoController;
            _imageDownloader = imageDownloader;
        }

        public void Init(ITitleListItem item)
        {
            _item = item;
        }

        public void DownloadImage(TitleInfo info)
        {
            _imgDownload?.Dispose();
            _imgDownload = _imageDownloader.DownloadImage(info, _imageSettings.ListItemWidth,
                _imageSettings.ListItemHeight, _item);
        }
        
        public void StopDownload()
        {
            _imgDownload?.Dispose();
        }

        public void ShowInfo(TitleInfo info)
        {
            _titleInfoController.ShowScreen(info);
        }
    }
}