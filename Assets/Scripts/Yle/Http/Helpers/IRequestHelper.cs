﻿using System;
using UnityEngine;
using Yle.Http.Queries;
using Yle.Http.Results;
using Yle.Settings;

namespace Yle.Http.Helpers
{
    public interface IRequest
    {
        IObservable<TitleQueryResult> GetTitle(string titleId);
        IObservable<SearchQueryResult> SearchItems(SearchQuery searchQuery);
        IObservable<Sprite> DownloadImage(string id, int width, int height);
    }
}