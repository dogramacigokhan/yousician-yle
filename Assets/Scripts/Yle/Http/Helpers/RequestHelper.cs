﻿using System;
using System.Text;
using Extensions;
using Newtonsoft.Json;
using RestSharp.Contrib;
using UniRx;
using UnityEngine;
using Yle.Http.Queries;
using Yle.Http.Results;
using Yle.Settings;

namespace Yle.Http.Helpers
{
    public class Request : IRequest
    {
        private readonly RequestSettings _requestSettings;

        public Request(RequestSettings requestSettings)
        {
            _requestSettings = requestSettings;
        }

        #region interface implementation

        public IObservable<TitleQueryResult> GetTitle(string titleId)
        {
            if (string.IsNullOrEmpty(titleId))
            {
                return Observable.Empty<TitleQueryResult>();
            }

            var titleUri = CreateTitleUri(titleId);
            return ObservableWeb.Get(titleUri.AbsoluteUri)
                .Select(Serialize<TitleQueryResult>);
        }

        public IObservable<SearchQueryResult> SearchItems(SearchQuery searchQuery)
        {
            if (searchQuery == null)
            {
                return Observable.Empty<SearchQueryResult>();
            }

            var searchUri = CreateSearchUri(searchQuery);
            return ObservableWeb.Get(searchUri.AbsoluteUri)
                .Select(Serialize<SearchQueryResult>);
        }

        public IObservable<Sprite> DownloadImage(string id, int width, int height)
        {
            if (string.IsNullOrEmpty(id))
            {
                return Observable.Empty<Sprite>();
            }
            
            var imageUri = CreateImageUri(id, width, height);
            Debug.Log(imageUri);
            return ObservableWeb.GetTexture(imageUri.AbsoluteUri)
                .Select(ConvertToSprite);
        }

        #endregion

        public Uri AppendAuthenticationInfo(Uri uri)
        {
            try
            {
                var newUri = uri.AddParameter(_requestSettings.AppIdKey, _requestSettings.AppId);
                return newUri.AddParameter(_requestSettings.AppKeyKey, _requestSettings.AppKey);
            }
            catch (Exception e)
            {
                Debug.LogException(e);
                return uri;
            }
        }

        public Uri CreateSearchUri(SearchQuery searchQuery)
        {
            try
            {
                // Construct uri
                var baseUri = new Uri(_requestSettings.BaseApiUrl);
                var uriBuilder = new UriBuilder(new Uri(baseUri, _requestSettings.SearchUrl));
                var parsedQuery = HttpUtility.ParseQueryString(uriBuilder.Query, Encoding.UTF8);

                // Append parameters
                if (!string.IsNullOrEmpty(searchQuery.Query))
                {
                    parsedQuery[_requestSettings.QueryKey] = searchQuery.Query;
                }

                if (searchQuery.Limit >= 0)
                {
                    parsedQuery[_requestSettings.LimitKey] = searchQuery.Limit.ToString();
                }

                if (searchQuery.Offset >= 0)
                {
                    parsedQuery[_requestSettings.OffsetKey] = searchQuery.Offset.ToString();
                }

                uriBuilder.Query = parsedQuery.ToString();
                return AppendAuthenticationInfo(uriBuilder.Uri);
            }
            catch (Exception e)
            {
                Debug.LogException(e);
                return null;
            }
        }

        public Uri CreateTitleUri(string titleId)
        {
            // Construct uri
            var baseUri = new Uri(_requestSettings.BaseApiUrl);
            var titleUri = new Uri(string.Format(_requestSettings.TitleUrlTemplate, titleId), UriKind.Relative);

            var uriBuilder = new UriBuilder(new Uri(baseUri, titleUri));
            return AppendAuthenticationInfo(uriBuilder.Uri);
        }

        public Uri CreateImageUri(string id, int width, int height)
        {
            return new Uri(string.Format(_requestSettings.ImageUrlTemplate, width, height, id));
        }

        private static T Serialize<T>(string result)
        {
            try
            {
                return string.IsNullOrEmpty(result) ? default(T) : JsonConvert.DeserializeObject<T>(result);
            }
            catch (Exception e)
            {
                Debug.LogException(e);
                return default(T);
            }
        }

        private static Sprite ConvertToSprite(Texture2D texture)
        {
            if (texture == null)
            {
                return null;
            }
            return Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0.5f, 0.5f));
        }
    }
}