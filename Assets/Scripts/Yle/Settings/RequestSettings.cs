﻿using System;
using UnityEngine;

namespace Yle.Settings
{
    [Serializable]
    public class RequestSettings
    {
        [Header("Connection settings")] public string BaseApiUrl;
        public string SearchUrl;
        public string TitleUrlTemplate;
        public string ImageUrlTemplate;

        [Header("Authentication settings")] public string AppId;
        public string AppKey;

        [Header("Url settings")] public string AppIdKey;
        public string AppKeyKey;
        public string QueryKey;
        public string LimitKey;
        public string OffsetKey;
    }
}