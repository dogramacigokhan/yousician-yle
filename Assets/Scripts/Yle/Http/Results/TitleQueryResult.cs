﻿using System;

namespace Yle.Http.Results
{
    [Serializable]
    public class TitleQueryResult : BaseQueryResult
    {
        public TitleInfo Data;
    }
}