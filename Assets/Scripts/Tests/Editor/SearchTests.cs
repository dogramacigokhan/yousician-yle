﻿using System.Collections.Generic;
using System.Text.RegularExpressions;
using Moq;
using NUnit.Framework;
using UnityEngine;
using Yle.Http.Helpers;
using Yle.Http.Results;
using Yle.Listing;
using Yle.Search;
using Zenject;

namespace Tests.Editor
{
    public class SearchTests : BaseUnitTextFixture
    {
        private static readonly List<string> ValidInputs = new List<string>
        {
            "test",
            "test-title",
            "test title",
            " test title",
            "test title "
        };
        
        private static readonly List<string> InvalidInputs = new List<string>
        {
            "",
            " ",
            "   "
        };

        [SetUp]
        public void SetUp()
        {
            // Setup the mock YleRequest with mock search result
            var searchQueryResult = MockSearchQueryResult().Object;
            var mockRequest = MockRequest(searchQueryResult);

            // Bind the mocked request
            Container.Bind<IRequest>().FromInstance(mockRequest.Object);

            // Bind yle search
            Container.Bind<ISearchBar>().FromMock().AsSingle();

            // Bind search controller
            Container.Bind<ITitleListController>().FromMock().AsSingle();
        }

        [TearDown]
        public void TearDown()
        {
            Container.UnbindAll();
        }

        [TestCaseSource(nameof(ValidInputs))]
        public void Search_TriggerSearch_OnValidInputChange(string text)
        {
            // Arrange
            var mockSearchController = MockSearchBarController(true);
            mockSearchController.Setup(s => s.Search(It.IsAny<string>()));

            // Act
            mockSearchController.Object.OnInputChange(text);

            // Assert
            mockSearchController.Verify(c => c.Search(It.IsAny<string>()), Times.Once);
        }
        
        [TestCaseSource(nameof(InvalidInputs))]
        public void Search_NotTriggerSearch_OnInvalidInputChange(string text)
        {
            // Arrange
            var mockSearchController = MockSearchBarController(true);
            mockSearchController.Setup(s => s.Search(It.IsAny<string>()));

            // Act
            mockSearchController.Object.OnInputChange(text);

            // Assert
            mockSearchController.Verify(c => c.Search(It.IsAny<string>()), Times.Never);
        }

        [Test]
        public void Search_ShowLoadingBar_OnInputChange()
        {
            // Arrange
            var mockSearch = Mock.Get(Container.Resolve<ISearchBar>());
            var mockSearchController = MockSearchBarController(true);
            mockSearchController.Setup(c => c.Search(It.IsAny<string>()));
            
            // Act
            mockSearchController.Object.OnInputChange("test");
            
            // Assert
            mockSearch.Verify(s => s.ToggleLoadingBar(true));
        }

        [Test]
        public void Search_HideLoadingBar_AfterSearch()
        {
            // Arrange
            var mockSearch = Mock.Get(Container.Resolve<ISearchBar>());
            var mockSearchController = MockSearchBarController(true);
            
            // Act
            mockSearchController.Object.OnSearchComplete(null);
            
            // Assert
            mockSearch.Verify(s => s.ToggleLoadingBar(false));
        }
        
        [Test]
        public void Search_CantContinueToSearch_IfThereAreNoPreviousSearches()
        {
            // Arrange
            var mockSearchController = MockSearchBarController(true);
            
            // Act
            mockSearchController.Object.ContinueSearch();
            
            // Assert
            UnityEngine.TestTools.LogAssert.Expect(LogType.Error, new Regex(".*"));
        }

        [TestCase(null)]
        [TestCase("")]
        public void Search_CantContinueToSearch_IfSearchIsNullOrEmpty(string search)
        {
            // Arrange
            var mockSearchController = MockSearchBarController(true);
            mockSearchController.Setup(c => c.OnSearchComplete(It.IsAny<SearchQueryResult>(), It.IsAny<bool>()));
            
            // Act
            mockSearchController.Object.Search(search);
            mockSearchController.Object.ContinueSearch();
            
            // Assert
            UnityEngine.TestTools.LogAssert.Expect(LogType.Error, new Regex(".*"));
        }
        
        [Test]
        public void Search_CanContinueToSearch()
        {
            // Arrange
            const string searchTerm = "valid_search";
            var mockSearchController = MockSearchBarController(true);
            mockSearchController.Setup(c => c.OnSearchComplete(It.IsAny<SearchQueryResult>(), It.IsAny<bool>()));
            
            // Act
            mockSearchController.Object.Search(searchTerm);
            mockSearchController.Object.ContinueSearch();
            
            // Assert
            UnityEngine.TestTools.LogAssert.NoUnexpectedReceived();
            mockSearchController.Verify(c => c.Search(searchTerm), Times.Exactly(2));
        }
    }
}