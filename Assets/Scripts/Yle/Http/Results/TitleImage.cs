﻿namespace Yle.Http.Results
{
    [System.Serializable]
    public class TitleImage
    {
        public string Id;
        public bool Available;
    }
}