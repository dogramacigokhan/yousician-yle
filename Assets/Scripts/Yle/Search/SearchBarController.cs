﻿using System;
using UniRx;
using UnityEngine;
using Yle.Http.Helpers;
using Yle.Http.Queries;
using Yle.Http.Results;
using Yle.Settings;

namespace Yle.Search
{
    public interface ISearchBarController
    {
        void OnInputChange(string text);
        void Search(string term);
        void ContinueSearch();
    }

    public class SearchBarController : ISearchBarController
    {
        private readonly ISearchBar _searchBar;
        private readonly IRequest _request;
        private readonly TitleListingSettings _titleListingSettings;

        private IDisposable _currentSearch;

        private string _searchTerm;
        private int _searchOffset;

        public SearchBarController(
            ISearchBar searchBar,
            IRequest request,
            TitleListingSettings titleListingSettings)
        {
            _searchBar = searchBar;
            _request = request;
            _titleListingSettings = titleListingSettings;
        }

        public virtual void OnInputChange(string text)
        {
            if (string.IsNullOrEmpty(text.Trim()))
            {
                return;
            }

            if (_searchBar != null)
            {
                // Show loading bar
                _searchBar.ToggleLoadingBar(true);
                _searchBar.ToggleClearButton(false);
            }

            _searchOffset = 0;
            Search(text);
        }

        public virtual void ContinueSearch()
        {
            if (string.IsNullOrEmpty(_searchTerm))
            {
                Debug.LogErrorFormat("Search term is null or empty, can't continue to search!");
                return;
            }

            _searchOffset += _titleListingSettings.ItemSearchLimit;
            Search(_searchTerm);
        }

        public virtual void Search(string term)
        {
            // Stop the current search
            _currentSearch?.Dispose();

            // Send search request on the thread pool
            _searchTerm = term;
            _currentSearch = _request
                .SearchItems(new SearchQuery(_searchTerm, _titleListingSettings.ItemSearchLimit, _searchOffset))
                .Subscribe(result => OnSearchComplete(result, _searchOffset == 0));
        }

        public virtual void OnSearchComplete(SearchQueryResult result, bool newSearch = true)
        {
            if (_searchBar != null)
            {
                _searchBar.ToggleLoadingBar(false);
                _searchBar.ToggleClearButton(true);
            }

            if (result != null)
            {
                result.NewSearch = newSearch;
            }
            MessageBroker.Default.Publish(result);
        }
    }
}