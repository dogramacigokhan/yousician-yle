﻿using System;
using Yle.Http.Helpers;
using Yle.Settings;

namespace Yle.InfoScreen
{
    public interface ITitleInfoController
    {
        void ShowScreen(Http.Results.TitleInfo info);
        void OnHideScreen();
        void StopDownload();
    }

    public class TitleInfoController : ITitleInfoController
    {
        private readonly ITitleInfoScreen _titleInfoScreen;
        private readonly ImageSettings _imageSettings;
        private readonly ImageDownloader _imageDownloader;

        private IDisposable _imgDownload;

        public TitleInfoController(
            ITitleInfoScreen titleInfoScreen,
            ImageSettings imageSettings,
            ImageDownloader imageDownloader)
        {
            _titleInfoScreen = titleInfoScreen;
            _imageSettings = imageSettings;
            _imageDownloader = imageDownloader;
        }
        
        public virtual void ShowScreen(Http.Results.TitleInfo info)
        {
            _titleInfoScreen.Show(info);
            DownloadImage(info);
        }

        public void OnHideScreen()
        {
            StopDownload();
        }

        public virtual void DownloadImage(Http.Results.TitleInfo info)
        {
            StopDownload();
            _imgDownload = _imageDownloader.DownloadImage(info, _titleInfoScreen.CoverImageWidth(),
                _imageSettings.CoverHeight, _titleInfoScreen);
        }

        public virtual void StopDownload()
        {
            _imgDownload?.Dispose();
        }
    }
}