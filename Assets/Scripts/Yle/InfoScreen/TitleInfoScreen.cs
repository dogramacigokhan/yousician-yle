﻿using DG.Tweening;
using Extensions;
using TMPro;
using UniRx;
using UnityEngine;
using UnityEngine.UI;
using Yle.Http.Helpers;
using Yle.Settings;
using Zenject;

namespace Yle.InfoScreen
{
    public interface ITitleInfoScreen : IImageSetter
    {
        void Show(Http.Results.TitleInfo info);
        int CoverImageWidth();
    }

    public class TitleInfoScreen : MonoBehaviour, ITitleInfoScreen
    {
        public Image CoverImage;
        public GameObject ImageNotAvailable;
        public Button BackButton;

        public TextMeshProUGUI Title;

        public TextMeshProUGUI Type;
        public TextMeshProUGUI AgeRestriction;
        public TextMeshProUGUI Duration;

        public TextMeshProUGUI Description;

        public TextMeshProUGUI OriginalTitle;
        public TextMeshProUGUI Country;

        private ITitleInfoController _controller;
        private ImageSettings _imageSettings;
        private ScreenSettings _screenSettings;
        private ResultHelper _resultHelper;

        private Tweener _fadeTween;
        private Sequence _visibilitySequence;

        private RectTransform _rectTransform;

        private RectTransform RectTransform =>
            _rectTransform ? _rectTransform : (_rectTransform = GetComponent<RectTransform>());

        [Inject]
        public void Construct(
            ITitleInfoController controller,
            ImageSettings imageSettings,
            ScreenSettings screenSettings,
            ResultHelper resultHelper)
        {
            _controller = controller;
            _imageSettings = imageSettings;
            _screenSettings = screenSettings;
            _resultHelper = resultHelper;
        }

        private void Awake()
        {
            // Listen for back button press
            BackButton.OnClickAsObservable()
                .Subscribe(_ => Hide())
                .AddTo(this);
            Observable.EveryUpdate()
                .Where(_ => Input.GetKeyDown(KeyCode.Escape))
                .Subscribe(_ => Hide())
                .AddTo(this);
        }
        
        public void Hide()
        {
            if (!gameObject.activeInHierarchy)
            {
                // Screen is already hidden, no need to hide
                return;
            }

            // Hide the game object
            ToggleVisibility(false);
            _controller.OnHideScreen();
        }

        private void SetInfo(Http.Results.TitleInfo info)
        {
            Title.text = _resultHelper.GetLocalizedValue(info.Title);

            Type.text = info.Type;
            AgeRestriction.text = ResultHelper.GetAgeRestriction(info.ContentRating);
            Duration.text = ResultHelper.Iso8601ToString(info.Duration);

            Description.text = _resultHelper.GetLocalizedValue(info.Description);

            OriginalTitle.text = _resultHelper.GetLocalizedValue(info.OriginalTitle, Title.text);
            Country.text = ResultHelper.GetCountry(info.CountryOfOrigin);
        }

        private void ToggleVisibility(bool to)
        {
            _visibilitySequence?.Kill();

            // Toggle visibility by animating the screen
            _visibilitySequence = RectTransform.SlideTransform(to, _screenSettings.ScreenAnimationDuration);
            _visibilitySequence.OnComplete(() => OnToggleVisibilityComplete(to));
            _visibilitySequence.Play();
        }

        private void OnToggleVisibilityComplete(bool to)
        {
            // Release the sprite resources on hide
            if (!to && CoverImage != null && CoverImage.sprite != null)
            {
                Destroy(CoverImage.sprite);
            }

            // Reset images
            CoverImage.SetAlpha(0f);
            ImageNotAvailable.SetActive(to && ImageNotAvailable.activeInHierarchy);
        }

        #region ITitleInfoScreen implementation

        public void Show(Http.Results.TitleInfo info)
        {
            if (info == null)
            {
                return;
            }

            SetInfo(info);
            ToggleVisibility(true);
        }

        public int CoverImageWidth()
        {
            return (int) CoverImage.rectTransform.rect.width;
        }
        
        #endregion

        #region IYleImageSetter implementation

        public void SetImage(Sprite sprite)
        {
            if (sprite != null && CoverImage != null)
            {
                CoverImage.sprite = sprite;
                _fadeTween = CoverImage.DOFade(1f, _imageSettings.FadeDuration);
            }

            ImageNotAvailable.SetActive(false);
        }

        public void SetImageAsNotAvailable()
        {
            _fadeTween?.Kill();
            if (CoverImage != null)
            {
                // Hide image
                CoverImage.SetAlpha(0f);
            }

            if (ImageNotAvailable != null)
            {
                // Show not available text
                ImageNotAvailable.SetActive(true);
            }
        }

        #endregion
    }
}