﻿using UnityEngine;
using UnityEngine.UI;

namespace Extensions
{
    public static class ImageExtensions
    {
        /// <summary>
        /// Sets the alpha value of image.
        /// </summary>
        /// <param name="image"></param>
        /// <param name="alpha">New alpha value to set</param>
        public static void SetAlpha(this Image image, float alpha)
        {
            if (image == null)
            {
                return;
            }
            image.color = new Color(image.color.r, image.color.g, image.color.b, alpha);
        }
    }
}