﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Yle.Http.Results
{
    [Serializable]
    public class SearchQueryResult : BaseQueryResult
    {
        public List<TitleInfo> Data;

        [JsonIgnore]
        public bool NewSearch;
    }
}