﻿using System;
using UnityEngine;

namespace Yle.Settings
{
    [Serializable]
    public class ImageSettings
    {
        [Header("List item images")] public int ListItemWidth;
        public int ListItemHeight;

        [Header("Cover images")] public int CoverHeight;

        [Header("Animation settings")] public int FadeDuration;
    }
}