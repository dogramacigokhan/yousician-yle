﻿namespace Yle.Http.Queries
{
    public class SearchQuery
    {
        public string Query { get; private set; }
        public int Limit { get; private set; }
        public int Offset { get; private set; }

        public SearchQuery(string query, int limit = 1, int offset = 0)
        {
            Query = query;
            Limit = limit;
            Offset = offset;
        }
    }
}