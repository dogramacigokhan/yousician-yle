﻿using TMPro;
using UniRx;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Yle.Search
{
    public interface ISearchBar
    {
        void ToggleLoadingBar(bool to);
        void ToggleClearButton(bool to);
    }
    
    public class SearchBar : MonoBehaviour, ISearchBar
    {
        public TMP_InputField InputField;
        public GameObject SearchLoadingBar;
        public Button ClearInputButton;

        private ISearchBarController _searchBarController;

        [Inject]
        public void Construct(ISearchBarController searchBarController)
        {
            _searchBarController = searchBarController;
        }

        private void Awake()
        {
            InputField.ObserveEveryValueChanged(i => i.text)
                .Subscribe(_searchBarController.OnInputChange);

            ClearInputButton.OnClickAsObservable()
                .Subscribe(_ => ClearInput());
        }

        private void ClearInput()
        {
            InputField.text = string.Empty;
            ClearInputButton.gameObject.SetActive(false);
        }

        public void ToggleLoadingBar(bool to)
        {
            SearchLoadingBar.SetActive(to);
        }

        public void ToggleClearButton(bool to)
        {
            ClearInputButton.gameObject.SetActive(!string.IsNullOrEmpty(InputField.text) && to);
        }
    }
}