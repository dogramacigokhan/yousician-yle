﻿using System;
using Newtonsoft.Json;

namespace Yle.Http.Results
{
    [Serializable]
    public class BaseQueryResult
    {
        public string ApiVersion;
        public ResultMeta Meta;
    }
}