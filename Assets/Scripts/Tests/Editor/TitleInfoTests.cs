﻿using System.Text.RegularExpressions;
using Moq;
using NUnit.Framework;
using UnityEngine;
using Yle.Http.Helpers;
using Yle.Http.Results;
using Yle.InfoScreen;
using Yle.Settings;
using Zenject;

namespace Tests.Editor
{
    public class TitleInfoTests : BaseUnitTextFixture
    {
        [SetUp]
        public void SetUp()
        {
            // Bind required interfaces and classes
            Container.Bind<IRequest>().FromMock().AsSingle();
            Container.Bind<ITitleInfoScreen>().FromMock().AsSingle();
            Container.Bind<ImageSettings>().FromMock().AsSingle();
            Container.Bind<ITitleInfoController>().FromMock().AsSingle();
            Container.Bind<ImageDownloader>().AsSingle();
        }

        [TearDown]
        public void TearDown()
        {
            Container.UnbindAll();
        }

        [Test]
        public void TitleInfo_CanShowInfoPage()
        {
            // Arrange
            var mockTitleInfoScreen = Mock.Get(Container.Resolve<ITitleInfoScreen>());
            var mockController = MockTitleInfoController(true);
            var mockTitleInfo = MockTitleInfo();

            mockController.Setup(c => c.DownloadImage(It.IsAny<TitleInfo>()));
            
            // Act
            mockController.Object.ShowScreen(mockTitleInfo.Object);
            
            // Assert
            mockTitleInfoScreen.Verify(i => i.Show(mockTitleInfo.Object), Times.Once);
            mockController.Verify(c => c.DownloadImage(mockTitleInfo.Object), Times.Once);
        }

        [Test]
        public void TitleInfo_SetNotAvailableImage_IfTitleInfoIsNull()
        {
            // Arrange
            var mockController = MockTitleInfoController(true);
            var mockTitleInfo = Mock.Get(Container.Resolve<ITitleInfoScreen>());
            
            // Act
            mockController.Object.DownloadImage(null);

            // Assert
            UnityEngine.TestTools.LogAssert.Expect(LogType.Warning, new Regex(".*"));
            mockTitleInfo.Verify(i => i.SetImageAsNotAvailable(), Times.Once);
        }
        
        [Test]
        public void TitleInfo_SetNotAvailableImage_IfSpriteIsNull()
        {
            // Arrange
            var mockTitleInfo = Mock.Get(Container.Resolve<ITitleInfoScreen>());
            var mockImageDownloader = MockImageDownloader(true);
            
            // Act
            mockImageDownloader.Object.OnDownloadComplete(null, mockTitleInfo.Object);

            // Assert
            UnityEngine.TestTools.LogAssert.Expect(LogType.Error, new Regex(".*"));
            mockImageDownloader.Verify(c => c.OnDownloadComplete(null, mockTitleInfo.Object));
            mockTitleInfo.Verify(i => i.SetImageAsNotAvailable(), Times.Once);
        }
        
        [Test]
        public void TitleInfo_SetNotAvailableImage_OnDownloadError()
        {
            // Arrange
            var mockTitleInfo = Mock.Get(Container.Resolve<ITitleInfoScreen>());
            var mockImageDownloader = MockImageDownloader(true);
            
            // Act
            mockImageDownloader.Object.OnDownloadError(null, mockTitleInfo.Object);

            // Assert
            UnityEngine.TestTools.LogAssert.Expect(LogType.Exception, new Regex(".*"));
            mockImageDownloader.Verify(c => c.OnDownloadError(null, mockTitleInfo.Object));
            mockTitleInfo.Verify(i => i.SetImageAsNotAvailable(), Times.Once);
        }
        
        [Test]
        public void TitleInfo_SetImage_OnDownloadComplete()
        {
            // Arrange
            var mockTitleInfo = Mock.Get(Container.Resolve<ITitleInfoScreen>());
            var mockImageDownloader = MockImageDownloader(true);

            var texture = Texture2D.blackTexture;
            var sprite = Sprite.Create(texture, Rect.zero, Vector2.zero);
            
            // Act
            mockImageDownloader.Object.OnDownloadComplete(sprite, mockTitleInfo.Object);

            // Assert
            mockImageDownloader.Verify(c => c.OnDownloadComplete(sprite, mockTitleInfo.Object));
            mockTitleInfo.Verify(i => i.SetImage(sprite), Times.Once);
            
            // Cleanup
            Object.DestroyImmediate(sprite);
        }
    }
}