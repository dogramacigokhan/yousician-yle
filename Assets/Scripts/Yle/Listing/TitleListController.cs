﻿using System;
using System.Collections.Generic;
using UniRx;
using UnityEngine;
using Yle.Http.Results;
using Yle.Search;

namespace Yle.Listing
{
    public interface ITitleListController
    {
        void AddItems(IEnumerable<TitleInfo> titleInfos);
        void RemoveItem(int index);
        void Clear();
        void LoadNewItems();
        void Destroy();
    }

    public class TitleListController : ITitleListController
    {
        private readonly TitleListItem.Pool _itemPool;
        public readonly List<ITitleListItem> Items = new List<ITitleListItem>();

        private readonly ITitleListScreen _titleListScreen;
        private readonly ISearchBarController _searchBarController;

        private bool _loadingNewItems;
        private IDisposable _searchListener;

        public TitleListController(
            TitleListItem.Pool itemPool,
            ITitleListScreen titleListScreen,
            ISearchBarController searchBarController)
        {
            _itemPool = itemPool;
            _titleListScreen = titleListScreen;
            _searchBarController = searchBarController;

            // Listen for the search results
            _searchListener = MessageBroker.Default.Receive<SearchQueryResult>().Subscribe(Receive);
        }
        
        public virtual void AddItems(IEnumerable<TitleInfo> titleInfos)
        {
            if (titleInfos == null)
            {
                return;
            }

            foreach (var titleInfo in titleInfos)
            {
                Items.Add(Spawn(titleInfo));
            }

            _titleListScreen.ToggleLoadingBar(false);
            _titleListScreen.ToggleVisibility(Items.Count > 0);

            // Enable loading items delayed to prevent aggressively load new items
            Observable.Timer(TimeSpan.FromSeconds(1f)).Take(1)
                .Subscribe(_ => _loadingNewItems = false);
        }

        public virtual void RemoveItem(int index = 0)
        {
            if (index >= Items.Count)
            {
                Debug.LogErrorFormat("Item with index: {0} is not found!", index);
                return;
            }

            var item = Items[index];
            Despawn(item);
            Items.RemoveAt(index);

            _titleListScreen?.ToggleVisibility(Items.Count > 0);
        }

        public virtual void Clear()
        {
            while (Items.Count > 0)
            {
                RemoveItem(0);
            }

            _titleListScreen.ResetScrollRect();
        }

        public virtual void LoadNewItems()
        {
            if (_loadingNewItems)
            {
                // Already loading new items
                return;
            }

            _loadingNewItems = true;
            _titleListScreen.ToggleLoadingBar(true);

            _searchBarController.ContinueSearch();
        }

        public void Destroy()
        {
            // Stop listening for search results
            _searchListener.Dispose();
        }

        protected virtual void Receive(SearchQueryResult searchQueryResult)
        {
            if (searchQueryResult == null)
            {
                return;
            }

            if (searchQueryResult.NewSearch)
            {
                // Clear the list on each new search
                Clear();
            }

            AddItems(searchQueryResult.Data);
        }

        public virtual ITitleListItem Spawn(TitleInfo titleInfo)
        {
            return _itemPool.Spawn(titleInfo);
        }

        public virtual void Despawn(ITitleListItem item)
        {
            _itemPool.Despawn(item);
        }
    }
}