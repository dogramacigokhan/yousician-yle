﻿using System.Collections.Generic;
using Moq;
using UniRx;
using UnityEngine;
using Yle.Http.Helpers;
using Yle.Http.Queries;
using Yle.Http.Results;
using Yle.InfoScreen;
using Yle.Listing;
using Yle.Search;
using Yle.Settings;
using Zenject;

namespace Tests.Editor
{
    public class BaseUnitTextFixture : ZenjectUnitTestFixture
    {
        protected virtual RequestSettings MockRequestSettings()
        {
            var settings = ScriptableObject.CreateInstance<YleSettings>();

            settings.Request = new RequestSettings
            {
                BaseApiUrl = "http://localhost",
                SearchUrl = "/some/search/url.json",
                TitleUrlTemplate = "/some/template/{0}.json",
                AppId = "testid",
                AppKey = "testkey",
                AppIdKey = "testidkey",
                AppKeyKey = "testkeykey",
                LimitKey = "testlim",
                OffsetKey = "testoffset",
                QueryKey = "testq"
            };
            return settings.Request;
        }

        protected virtual Mock<IRequest> MockRequest(SearchQueryResult result)
        {
            var mockRequest = new Mock<IRequest>();
            mockRequest.Setup(r => r.SearchItems(It.IsAny<SearchQuery>()))
                .Returns(Observable.Create<SearchQueryResult>(observer =>
                {
                    observer.OnNext(result);
                    return Disposable.Empty;
                }));
            return mockRequest;
        }

        protected virtual Mock<SearchQueryResult> MockSearchQueryResult()
        {
            var mockSearchResult = new Mock<SearchQueryResult>();
            mockSearchResult.SetupAllProperties();
            mockSearchResult.Object.Data = new List<TitleInfo>
            {
                new TitleInfo {Id = "5"},
                new TitleInfo {Id = "10"},
            };

            return mockSearchResult;
        }

        protected virtual Mock<SearchBarController> MockSearchBarController(bool callBase = false)
        {
            var yleSearch = Container.Resolve<ISearchBar>();
            var request = Container.Resolve<IRequest>();
            var settings = new TitleListingSettings();

            return new Mock<SearchBarController>(yleSearch, request, settings)
            {
                CallBase = callBase
            };
        }

        protected virtual Mock<TitleInfo> MockTitleInfo()
        {
            var mock = new Mock<TitleInfo>();
            mock.SetupAllProperties();

            mock.Object.Id = "test-id";
            mock.Object.Title = new Dictionary<string, string>
            {
                {"test-key", "test-value"}
            };

            return mock;
        }

        protected virtual Mock<TitleListController> MockTitleListController(TitleInfo titleInfo,
            bool callBase = false)
        {
            var itemPool = Container.Resolve<TitleListItem.Pool>();
            var titleList = Container.Resolve<ITitleListScreen>();
            var searchController = Container.Resolve<ISearchBarController>();

            var listControllerMock =
                new Mock<TitleListController>(itemPool, titleList, searchController) {CallBase = callBase};
            listControllerMock.SetupAllProperties();

            // Mock spawn & despawn methods in unit test
            var toSpawn = Container.Resolve<ITitleListItem>();
            toSpawn.TitleInfo = titleInfo;

            listControllerMock.Setup(m => m.Spawn(It.IsAny<TitleInfo>())).Returns(toSpawn);
            listControllerMock.Setup(m => m.Despawn(It.IsAny<ITitleListItem>()));

            return listControllerMock;
        }

        protected virtual Mock<TitleListItemController> MockTitleListItemController(bool callBase = false)
        {
            var settings = new ImageSettings();
            var infoController = Container.Resolve<ITitleInfoController>();
            var imageDownloader = Container.Resolve<ImageDownloader>();

            var mock = new Mock<TitleListItemController>(settings, infoController, imageDownloader)
            {
                CallBase = callBase
            };
            mock.SetupAllProperties();

            var item = Container.Resolve<ITitleListItem>();
            mock.Object.Init(item);

            return mock;
        }

        protected virtual Mock<TitleInfoController> MockTitleInfoController(bool callBase = false)
        {
            var titleInfo = Container.Resolve<ITitleInfoScreen>();
            var settings = new ImageSettings();
            var imageDownloader = Container.Resolve<ImageDownloader>();

            return new Mock<TitleInfoController>(titleInfo, settings, imageDownloader) {CallBase = callBase};
        }

        protected virtual Mock<ImageDownloader> MockImageDownloader(bool callBase = false)
        {
            var request = Container.Resolve<IRequest>();
            return new Mock<ImageDownloader>(request) {CallBase = callBase};
        }
    }
}