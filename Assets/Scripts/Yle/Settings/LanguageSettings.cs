﻿using System;

namespace Yle.Settings
{
    [Serializable]
    public class LanguageSettings
    {
        public string DefaultLanguageCode;
        public string FallbackLanguageCode;
    }
}