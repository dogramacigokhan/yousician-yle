using UnityEngine;
using Yle.Http;
using Yle.Http.Helpers;
using Yle.Http.Results;
using Yle.InfoScreen;
using Yle.Listing;
using Yle.Search;
using Yle.Settings;
using Zenject;

namespace Yle
{
    public class YleInstaller : MonoInstaller<YleInstaller>
    {
        public Transform YleTitleListParent;

        [Inject] private YleSettings _settings;

        public override void InstallBindings()
        {
            // Request related bindings
            Container.BindInterfacesAndSelfTo<Request>().AsSingle();

            // Search related bindings
            Container.BindInterfacesAndSelfTo<SearchBarController>().AsSingle();

            // Title listing
            Container.BindInterfacesAndSelfTo<TitleListController>().AsSingle();
            Container.BindInterfacesAndSelfTo<TitleListItemController>().AsTransient();

            // Memory pool binding for title items
            Container.BindMemoryPool<ITitleListItem, TitleListItem.Pool>()
                .To<TitleListItem>()
                .FromComponentInNewPrefab(_settings.TitleListing.TitleListItemPrefab)
                .UnderTransform(YleTitleListParent);
            
            // Title info
            Container.BindInterfacesAndSelfTo<TitleInfoController>().AsSingle();
            
            // Helpers
            Container.Bind<ResultHelper>().AsSingle();
            Container.BindInterfacesAndSelfTo<ImageDownloader>().AsSingle();
        }
    }
}