﻿using System;

namespace Yle.Http.Results
{
    [Serializable]
    public class ResultMeta
    {
        public int Offset;
        public int Limit;
        public int Count;
        public int Program;
        public int Clip;
        public string Q;
    }
}