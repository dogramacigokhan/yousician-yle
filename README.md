# YLE Search Application

## About

YLE Search is a simple application to search titles (TV Shows, Movies, Radio Programs, videos etc.) in YLE database, list results in a scroll view and show detailed information about selected titles.

> **Note:** Preferred screen resolution is 9:16 (Portrait Mode) but GUI is designed to support all resolutions including the Landscape Modes.

## Workflow

First of all, I designed the GUI for all screens which I was going to use. Then I played with the YLE API using Postman to test the requests and see the sample results. After getting used to the API, I had worked on the technical design of the project.

After the design phase, I created the required classes and interfaces without any implementation. Then I wrote simple unit tests for these non-implemented methods and then moved on to the implementation. Details of this process, used plugins and other methodologies are given in the technical implementation section.

## GUI Design

UI design is inspired by the Netflix mobile application as the YLE search results are pretty similar to the Netflix results. I took several screenshots from the Netflix mobile application like the search screen, search result screen and the details screen:

|              Search Screen               |           Search Results Screen            |            Title Info Screen            |
| :--------------------------------------: | :----------------------------------------: | :-------------------------------------: |
| ![](./ReadmeResources/Ex_SearchMain.jpg) | ![](./ReadmeResources/Ex_SearchResult.jpg) | ![](./ReadmeResources/Ex_TitleInfo.jpg) |

I've imported these screenshots into Unity and created a color preset out of used colors. Color preset is created in the project scope to make it persistent across different developers:

![Netflix Color Preset](./ReadmeResources/ColorPreset.jpg)



Then I created similar user interfaces by finding similar icons, scrollbars, images and then positioning them similar to the Netflix app by using the original colors in the preset. Here is my search screen and the actual Netflix search screen comparison:

![NetflixSimilarity](./ReadmeResources/NetflixSimilarity.gif)



## Animations & Effects

Several techniques are used to achieve certain effects. These are listed below.

### Circular loading bar animation during search

This was a quite simple animation to rotate the circular progress bar, so it could be done with either Unity's built-in animation system or with DOTween. I chose to do it with built-in animation system as it won't require any parameter in runtime.

### Image fade animations

Images are faded-in when they are loaded from the server. This animation is done using DOTween as it requires the fade-duration parameter from my settings (ScriptableObject) file in runtime.

### Title Info Screen animation

There is a slide animation in Title Info Screen whenever user clicks on a title in the search results screen. It has a basic slide animation and it's implemented using DOTween instead of the built-in animation system as the animation requires the screen width parameter in runtime.

### Title cover image soft mask

Unfortunately, uGUI does not support soft masks by default. The provided masking components are only for hard masks. I've imported the [UnityUIExtensions](https://bitbucket.org/UnityUIExtensions/unity-ui-extensions) library which includes lots of helpful scripts including the soft-mask script. I've created a simple alpha mask image in Photoshop and soft-masked the cover images in Title Info screen like Netflix did:

![](./ReadmeResources/HardMask.jpg) + ![](./ReadmeResources/Mask.jpg) = ![](./ReadmeResources/SoftMask.jpg)



## Used Plugins, Frameworks & Helpers

There are several plugins, frameworks and helper classes that I've used in this project and the technical design is affected by some of them. So, let's list them first.

### DOTween

It is used to tween/animate objects.

### TextMeshPro

This is one of my must-have plugins. It has the ability to render texts as crisp as possible no matter the font size is and it has tons of options while rendering the texts.

### UnityUIExtensions

It is used to soft-mask cover images in Title Info screen. There are also lots of helpful scripts in this extension set which can be used for other UI improvements such as the ready-to-use Re-orderable Lists component which, for example, can be used in Watch list or Favorites screens to order titles easily.

### UniRx

This project is developed using reactive programming paradigm hence I've imported the Reactive Extensions for Unity. I've used other features that UniRx provides such as the MessageBroker class, global coroutine scheduler class, and other Unity specific extensions methods which converts standard methods to observables.

### Zenject

Zenject might be overkill for small demo projects but since I've developed this project as a ready to release product, I had to use a proper dependency injection system to create a testable, robust and easily improvable system.

### Moq

I've used Moq library (which I was already familiar with) to mock and unit test my classes and interfaces. Latest version of the Moq already comes with Zenject framework and Zenject has a few extension methods to support mocking while binding new types.

### Newtonsoft.Json

Even though I'm familiar with Netwonsoft.Json in my previous experiences, I'd normally like to use built-in JsonUtility tool to serialize or deserialize the data because it's using the internal serialization and deserialization system of Unity. But the problem is, the default serialization is so limited in Unity as it can't serialize properties, dictionaries, classes without Serializable attribute. And the API of the JsonUtility class is so limited.

With Newtonsoft.Json, I could serialize anything, modify/alter the serialization process by providing lots of useful attributes to the data classes. This is the reason that I've chosen to use Netwonsoft.Json instead of the JsonUtility.

## Technical Design & Implementation

There were few classes needed to develop this project as the scope of this project was pretty narrow. First, I've listed them in the design phase and created a simple diagram to visualize the interaction between them:

![](./ReadmeResources/InitialDesign.svg)

### TDD in Unity

I've developed this project using TDD (Test Driven Development) methodologies. Normally, TDD is pretty straightforward if you are developing an application outside of Unity. It involves creating dummy classes with non-implemented methods, writing failing unit tests to test the small parts (units) of your application, and then implementing the methods to pass the tests and doing this in a loop until the project is completed.

But in Unity, we have MonoBehaviour classes, GameObjects, Transforms, UI elements which are referenced before running the application and we can't just mock all of these to write unit tests. So I had to abstract the logic from the MonoBehaviour classes to related controller classes.

#### Example for the abstraction of behaviors and controllers

![](./ReadmeResources/AbstractionImplemented.svg)

Image above depicts an example case for the design of this project. Classes that are marked with * are MonoBehaviour classes, which are responsible for containing Unity Engine related types, methods to show & hide this elements, and animate them if necessary. Controller and helper classes are responsible for the logic of the application. All MonoBehaviour classes, controller classes and helper classes have their own interface to ease the testing process. 

With the help of this abstraction, I was able to easily test the logic in my controller and helper classes by mocking them and also mocking the interfaces of my MonoBehaviour classes.

### Web Requests

As I was developing this project using UniRx, I decided to make web requests using observable calls. UniRx has [ObservableWWW](https://github.com/neuecc/UniRx#network-operations) class to make asynchronous web requests using observables. But this class uses deprecated WWW class to make the calls instead of new [UnityWebRequest](https://docs.unity3d.com/ScriptReference/Networking.UnityWebRequest.html) class. So I've used another implementation for the web request: [ObservableWeb](https://github.com/kado-yasuyuki/UniRx/blob/feature/UnityWebRequest-AsObservable-%23101/Assets/Plugins/UniRx/Scripts/UnityEngineBridge/ObservableWeb.cs), which is implemented by another developer in a [feature branch](https://github.com/kado-yasuyuki/UniRx/tree/feature/UnityWebRequest-AsObservable-%23101).

With the help of ObservableWeb class, I made the observable web requests to get results from the Yle API and downloaded images:

```c#
public IObservable<Sprite> DownloadImage(string id, ImageSettings.AspectRatio aspectRatio, int width)
{
    if (string.IsNullOrEmpty(id))
    {
        return Observable.Empty<Sprite>();
    }
    
    var imageUri = CreateImageUri(id, aspectRatio, width);
    return ObservableWeb.GetTexture(imageUri.AbsoluteUri)
        .Select(ConvertToSprite);
}
```

### Pooling the search results

Search result items are shown in a scroll view list and whenever a new search is made, old items in this list should be cleared and new items should be instantiated. Instead of instantiating & destroying these objects, I've decided to use pooling mechanism. 

Zenject has built-in support for [Memory Pools](https://github.com/modesttree/Zenject/blob/master/Documentation/MemoryPools.md). Since I was using Zenject as the dependency injection framework, I decided to use this built-in system instead of re-implementing a new pooling mechanism or using another plugin. It is also quite easy to add pooling support for MonoBehaviour and Non-MonoBehaviour classes in Zenject, and it allows you to pass lots of useful parameters:

```c#
// Memory pool binding for title items
Container.BindMemoryPool<ITitleListItem, TitleListItem.Pool>()
    .To<TitleListItem>()
    .FromComponentInNewPrefab(_settings.TitleListing.TitleListItemPrefab)
    .UnderTransform(YleTitleListParent);
```

### Settings

There are lots of options to store settings in a project, or in a game. They can be stored as *Json*, *INI* or as a plain text. But Unity has ScriptableObject class which is a perfect candidate for storing the settings without parsing *Json*, *INI* or requiring any other third party tool. In this project all settings are stored in a ScriptableObject asset which is located at **Settings/YleSettings**.

```c#
[CreateAssetMenu(fileName = "YleSettings", menuName = "Settings/Yle Settings")]
public class YleSettings : ScriptableObjectInstaller<YleSettings>
{
    public RequestSettings Request;
    public TitleListingSettings TitleListing;
    public ImageSettings Image;
    public ScreenSettings Screen;
    public LanguageSettings Language;
    // ...
}
```

Zenject also has support for [ScriptableObjects](https://github.com/modesttree/Zenject#scriptable-object-installer). By storing all of my settings as a ScriptableObject asset and with the help of Zenject bindings, I was able to access my settings as any other dependency:

```c#
public class ResultHelper
{
    // languageSettings is injected automatically by Zenject
    private readonly LanguageSettings _languageSettings;
    public ResultHelper(LanguageSettings languageSettings)
    {
        _languageSettings = languageSettings;
    }
    // ...
}
```

### Localization support

Yle API returns some results for multiple languages, such as the title result:

```javascript
title: {
    fi: "Kolme muskettisoturia",
    sv: "De tre musketörerna"
}
```

In **Settings/YleSettings/Language Settings**, there are **Default Language Code** and **Fallback Language Code** fields that can be used to define primary and fallback language for this type of multi-language results. If none of these language codes are found in the result, first value of the result will be shown.