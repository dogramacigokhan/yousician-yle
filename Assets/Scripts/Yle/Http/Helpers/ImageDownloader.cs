﻿using System;
using UniRx;
using UnityEngine;
using Yle.Http.Results;
using Yle.Settings;
using Disposable = UniRx.Disposable;

namespace Yle.Http.Helpers
{
    public interface IImageSetter
    {
        void SetImage(Sprite sprite);
        void SetImageAsNotAvailable();
    }

    public class ImageDownloader
    {
        private readonly IRequest _request;

        public ImageDownloader(IRequest request)
        {
            _request = request;
        }

        public virtual IDisposable DownloadImage(TitleInfo titleInfo, int width, int height, IImageSetter imageSetter)
        {
            if (string.IsNullOrEmpty(titleInfo?.Image?.Id) || !titleInfo.Image.Available)
            {
                OnImageNotFound(titleInfo, imageSetter);
                return Disposable.Empty;
            }

            return _request.DownloadImage(titleInfo.Image.Id, width, height)
                .Subscribe(s => OnDownloadComplete(s, imageSetter), e => OnDownloadError(e, imageSetter));
        }

        public virtual void OnImageNotFound(TitleInfo title, IImageSetter imageSetter)
        {
            Debug.LogWarningFormat("Image not found for title: {0}", title?.ToString() ?? "Unknown title");
            imageSetter.SetImageAsNotAvailable();
        }

        public virtual void OnDownloadComplete(Sprite sprite, IImageSetter imageSetter)
        {
            if (sprite == null)
            {
                Debug.LogError("Downloaded sprite is null for the title!");
                imageSetter.SetImageAsNotAvailable();
            }
            else
            {
                imageSetter.SetImage(sprite);
            }
        }

        public virtual void OnDownloadError(Exception exception, IImageSetter imageSetter)
        {
            Debug.LogException(exception ?? new Exception("Unknown exception"));
            imageSetter.SetImageAsNotAvailable();
        }
    }
}