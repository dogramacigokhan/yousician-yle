﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Yle.Http.Results
{
    [Serializable]
    public class TitleInfo
    {
        public string Id;
        
        public Dictionary<string, string> Title;
        public Dictionary<string, string> ItemTitle;
        public Dictionary<string, string> OriginalTitle;
        
        public Dictionary<string, string> Description;
        public ItemContentRating ContentRating;

        public string[] CountryOfOrigin;

        public TitleImage Image;
        public string TypeMedia;
        public string Type;
        public string Duration;

        public override string ToString()
        {
            return "{" + string.Join(",", Title.Select(kv => kv.Key + "=" + kv.Value).ToArray()) + "}";
        }
    }

    [Serializable]
    public class ItemContentRating
    {
        public Dictionary<string, string> Title;
        public int AgeRestriction;
        public string RatingSystem;
    }
}