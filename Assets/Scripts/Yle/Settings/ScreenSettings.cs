﻿using System;

namespace Yle.Settings
{
    [Serializable]
    public class ScreenSettings
    {
        public float ScreenAnimationDuration;
    }
}