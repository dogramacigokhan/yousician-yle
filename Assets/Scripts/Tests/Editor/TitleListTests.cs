﻿using System.Linq;
using NUnit.Framework;
using Yle.Listing;
using Yle.Search;
using Zenject;

namespace Tests.Editor
{
    public class TitleListTests : BaseUnitTextFixture
    {
        [SetUp]
        public void SetUp()
        {
            // Bind title list interface and related controller
            Container.Bind<ITitleListScreen>().FromMock().AsSingle();
            Container.Bind<ITitleListItem>().FromMock().AsTransient();

            Container.Bind<TitleListItem.Pool>().FromMock().AsSingle();
            Container.Bind<ISearchBarController>().FromMock().AsSingle();
        }

        [TearDown]
        public void TearDown()
        {
            Container.UnbindAll();
        }

        [Test]
        public void TitleList_CanAddNewItems()
        {
            // Arrange
            var titleInfo = MockTitleInfo().Object;
            var listController = MockTitleListController(titleInfo, true).Object;

            // Act
            listController.AddItems(new[] {titleInfo});

            // Assert
            Assert.That(listController.Items.Count, Is.EqualTo(1));
            Assert.That(listController.Items.First().TitleInfo.Id, Is.EqualTo(titleInfo.Id));
        }

        [Test]
        public void TitleList_CanRemoveItem()
        {
            var titleInfo = MockTitleInfo().Object;
            var listController = MockTitleListController(titleInfo, true).Object;

            // Act
            listController.AddItems(new[] {titleInfo});
            Assert.That(listController.Items.Count, Is.EqualTo(1));

            // Assert
            listController.RemoveItem(0);
            Assert.That(listController.Items.Count, Is.EqualTo(0));
        }

        [Test]
        public void TitleList_CanCleanItems()
        {
            var titleInfo = MockTitleInfo().Object;
            var listController = MockTitleListController(titleInfo, true).Object;

            // Act
            listController.AddItems(new[] {titleInfo, titleInfo, titleInfo});
            Assert.That(listController.Items.Count, Is.EqualTo(3));

            // Assert
            listController.Clear();
            Assert.That(listController.Items.Count, Is.EqualTo(0));
        }
    }
}