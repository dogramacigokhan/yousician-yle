﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.UI.Extensions;

namespace Extensions
{
    public static class RectTransformAnimExtensions
    {
        /// <summary>
        /// Slides the given transform from right edge of the screen to the center.
        /// </summary>
        /// <param name="t"></param>
        /// <param name="to">Parameter to show or hide the transform</param>
        /// <param name="duration">Animation duration</param>
        public static Sequence SlideTransform(this RectTransform t, bool to, float duration)
        {
            var rect = t.GetComponentInParent<Canvas>().GetComponent<RectTransform>();
            var sequence = DOTween.Sequence();
            
            sequence
                .Append(t.DOLocalMoveX(to ? 0 : rect.rect.width, duration))
                .OnStart(() =>
                {
                    t.localPosition = to ? new Vector3(rect.rect.width, 0f) : Vector3.zero;
                    t.gameObject.SetActive(true);
                })
                .OnComplete(() => t.gameObject.SetActive(to));
            return sequence;
        }
    }
}